Title: okteta update needs --permit-downgrade
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2018-06-14
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/okteta[>=4]

kde/okteta is no longer released with KDE Applications on the request of its
maintainer [1.] Because of this its version changed from yy.mm.i (e.g.
17.08.3) to currently 0.25.0 requiring you to pass "--permit-downgrade" to
your cave invocation when updating:

# cave resolve -1 kde/okteta --permit-downgrade kde/okteta

[1]: https://mail.kde.org/pipermail/release-team/2018-March/010871.html
