# Copyright 2010 Ingmar Vanhassel
# Copyright 2014, 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=phonon-backend-vlc-${PV}

require kde.org [ subdir=phonon/phonon-backend-vlc/${PV} ]
require cmake [ api=2 ] freedesktop-mime

export_exlib_phases src_configure src_compile src_install

SUMMARY="A VLC-based phonon backend"
HOMEPAGE="https://commits.kde.org/${PN}"

LICENCES="LGPL-3"
SLOT="0"
MYOPTIONS="
    qt4
    qt5 [[ description = [ Build against Qt5 instead of Qt4 (experimental) ] ]]
    ( qt4 qt5 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build+run:
        media/vlc[>=2.1.0][dbus(+)]
        media-libs/phonon[>=4.8.50][qt4?][qt5?]
        qt4? ( x11-libs/qt:4[>=4.4.0][dbus][phonon] )
        qt5? (
            media/vlc[-qt4(-)]
            x11-libs/qtbase:5
        )
"

ever is_scm || CMAKE_SOURCE=${WORKBASE}/${PNV}

phonon-vlc_src_configure() {
    if option qt4; then
        edo mkdir "${WORKBASE}"/qt4-build
        edo pushd "${WORKBASE}"/qt4-build
        ecmake -DPHONON_BUILD_PHONON4QT5:BOOL=FALSE
        edo popd
    fi
    if option qt5; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        ecmake -DPHONON_BUILD_PHONON4QT5:BOOL=TRUE
        edo popd
    fi
}

phonon-vlc_src_compile() {
    if option qt4; then
        edo pushd "${WORKBASE}"/qt4-build
        emake
        edo popd
    fi
    if option qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        emake
        edo popd
    fi
}

phonon-vlc_src_install() {
    if option qt4; then
        edo pushd "${WORKBASE}"/qt4-build
        emake -j1 DESTDIR="${IMAGE}" install
        edo popd
    fi
    if option qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        emake -j1 DESTDIR="${IMAGE}" install
        edo popd
    fi
}

