# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2010 Ingmar Vanhassel
# Copyright 2013-2014, 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'phonon-4.2-scm.kdebuild-1' from Genkdesvn, which is:
#     Copyright 2007-2008 by the individual contributors of the genkdesvn project
#     Based in part upon the respective ebuild in Gentoo which is:
#     Copyright 1999-2008 Gentoo Foundation

require kde.org cmake [ api=2 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="KDE multimedia API"
HOMEPAGE="https://phonon.kde.org/"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
SLOT="0"
MYOPTIONS="doc
    gstreamer  [[ description = [ Enable the Phonon Gstreamer backend ] ]]
    pulseaudio [[ description = [ Enable playback through Pulseaudio ] ]]
    qt4
    qt5        [[ description = [ Build against Qt5 instead of Qt4 (experimental) ] ]]
    vlc        [[ description = [ Use the Phonon VLC backend, which is the recommended backend ] ]]

    ( gstreamer vlc ) [[ number-selected = at-least-one ]]
    ( qt4 qt5 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
        virtual/pkg-config
        qt5? ( kde-frameworks/extra-cmake-modules[>=1.7.0] )
    build+run:
        pulseaudio? (
            dev-libs/glib:2
            media-sound/pulseaudio[>=0.9.21]
        )
        qt4? (
            x11-libs/qt:4[>=4.7.4-r1][dbus][phonon] [[
                note = [ Requires >=4.6.1 but 4.7.4-r1 added some multilib fixes
                         that are needed when multibuild is enabled ] ]]
        )
        qt5? (
            x11-libs/qtbase:5
            x11-libs/qttools:5
        )
    post:
        gstreamer? ( media-libs/phonon-gstreamer )
        vlc? ( media-libs/phonon-vlc )
"

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'doc PHONON_BUILD_DOC'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
        'pulseaudio GLIB2'
        'pulseaudio PulseAudio'
)
CMAKE_SRC_CONFIGURE_PARAMS=(
    # automoc is included in cmake since 2.8.5
    -DCMAKE_AUTOMOC:BOOL=TRUE
    -DCMAKE_INSTALL_DATAROOTDIR:PATH=/usr/share
    # super experimental QML support
    -DPHONON_BUILD_DECLARATIVE_PLUGIN:BOOL=FALSE
    -DPHONON_BUILD_DESIGNER_PLUGIN:BOOL=TRUE
    -DPHONON_INSTALL_QT_EXTENSIONS_INTO_SYSTEM_QT:BOOL=TRUE
)

_ecmake_helper() {
    ecmake \
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${CMAKE_SRC_CONFIGURE_OPTIONS[@]}" ; do
            cmake_option ${s}
        done ) \
        $(for s in "${CMAKE_SRC_CONFIGURE_OPTION_WITHS[@]}" ; do
            cmake_with ${s}
        done ) \
        "$@"
}

phonon_src_configure() {
    if option qt4 ; then
        edo mkdir "${WORK}"/qt4-build
        edo pushd "${WORK}"/qt4-build
        # libqzeitgeist is unmaintained and not ported to Qt5
        _ecmake_helper \
            -DPHONON_BUILD_PHONON4QT5:BOOL=FALSE \
            -DWITH_QZeitgeist:BOOL=FALSE
        edo popd
    fi

    if option qt5 ; then
        edo mkdir "${WORK}"/qt5-build
        edo pushd "${WORK}"/qt5-build
        # Qt5Declarative (aka qtquick1) is only recommended for "the super
        # experimental QML support" and it's deprecated with Qt 5.6
        _ecmake_helper \
            -DPHONON_BUILD_PHONON4QT5:BOOL=TRUE \
            -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Declarative:BOOL=TRUE
        edo popd
    fi
}

phonon_src_compile() {
    if option qt4 ; then
        edo pushd "${WORK}"/qt4-build
        emake
        edo popd
    fi
    if option qt5 ; then
        edo pushd "${WORK}"/qt5-build
        emake
        edo popd
    fi
}

phonon_src_install() {
    if option qt4; then
        edo pushd "${WORK}"/qt4-build
        emake -j1 DESTDIR="${IMAGE}" install
    fi
    if option qt5; then
        edo pushd "${WORK}"/qt5-build
        emake -j1 DESTDIR="${IMAGE}" install
    fi
}

