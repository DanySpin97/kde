# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde [ translations='qt' ]

SUMMARY="QtQuick plugins to build user interfaces based on the KDE UX guidelines"

LICENCES="LGPL-2"
MYOPTIONS="examples
    plasma [[ description = [ Build and install the Plasma style ] ]]
"

KF5_MIN_VER=5.18.0
QT_MIN_VER=5.5.0

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools [[ note = [ lconvert and lrelease ] ]]
    build+run:
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        plasma? ( kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}] )
    run:
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
        examples? ( kde-frameworks/breeze-icons:5 )
"

CMAKE_SRC_CONFIGURE_OPTIONS+=( 'plasma PLASMA_ENABLED' )
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( EXAMPLES )
CMAKE_SRC_CONFIGURE_PARAMS+=(
    # If examples is enabled it tries to fetch breeze icons from git. To avoid
    # this we can use the installed ones from kde-frameworks/breeze-icons:5.
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DBREEZEICONS_DIR=/usr/share/icons/breeze
)

