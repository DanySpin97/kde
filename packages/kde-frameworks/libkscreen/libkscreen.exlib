# Copyright 2013,2015-2018 Heiko Becker <heirecka@exherbo.org>
# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde

SUMMARY="KDE's screen management library"

LICENCES="GPL-2"
SLOT="5"
MYOPTIONS+="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

if ever at_least 5.13.90 ; then
    KF5_MIN_VER="5.50.0"
    QT_MIN_VER=5.11.0
else
    KF5_MIN_VER="5.42.0"
    QT_MIN_VER=5.9.0
fi

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        x11-libs/libxcb   [[ note = [ Could be optional with 5.6.x ] ]]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        !kde-frameworks/kscreen:0 [[
            description = [ Moved to slot 5 to match other frameworks ]
            resolution = uninstall-blocked-after
         ]]
"

# Tests need a running X server (1.0.2)
RESTRICT=test

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

