# Copyright 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ branch='develop' unstable=true subdir=${PN}/${PV}/src ] kde

export_exlib_phases src_test

SUMMARY="Sink is a data access layer handling synchronization, caching and indexing"

LICENCES="LGPL-2.1 GPL-2"
SLOT="0"
MYOPTIONS="
    dav [[ description = [ Build a DAV resource to sync calendars and contacts ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-db/lmdb[>=0.9]
        dev-db/xapian-core[>=1.4]
        dev-libs/flatbuffers[>=1.4.0]
        kde/kasync[>=0.1.2]
        kde/kimap2[>=0.2]
        kde-frameworks/kcalcore:5
        kde-frameworks/kcontacts:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kmime:5
        net-misc/curl
        x11-libs/qtbase:5
        dav? ( kde/kdav2 )
    test:
        dev-scm/libgit2[>=0.20.0]
"

BUGS_TO="heirecka@exherbo.org"

# Tests a) only work if they are installed and b) most of them need X
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Disable -Werror
    -DCATCH_ERRORS:BOOL=FALSE
    -DENABLE_ASAN:BOOL=FALSE
    -DENABLE_TSAN:BOOL=FALSE
    # Would need valgrind
    -DENABLE_MEMCHECK:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( DAV )
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DCMAKE_DISABLE_FIND_PACKAGE_Libgit2:BOOL=FALSE -DCMAKE_DISABLE_FIND_PACKAGE_Libgit2:BOOL=TRUE'
)

sink_src_test() {
    echo "TEMP: ${TEMP}|"
    esandbox allow_net "unix:${TEMP}/*"
    esandbox allow_net --connect "unix:${TEMP}/*"

    default

    esandbox disallow_net --connect "unix:${TEMP}/*"
    esandbox disallow_net "unix:${TEMP}/*"
}

