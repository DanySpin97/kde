# Copyright 2008,2010 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2013-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'akonadi-server-scm.kdebuild-1' from Genkdesvn, which is:
# Copyright 2007-2008 by the individual contributors of the genkdesvn project
# Based in part upon the respective ebuild in Gentoo which is:
# Copyright 1999-2008 Gentoo Foundation

require kde-apps kde [ translations='ki18n' ]
require test-dbus-daemon freedesktop-mime

export_exlib_phases src_configure

SUMMARY="The server part of Akonadi"
HOMEPAGE="http://www.akonadi-project.org"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    LGPL-2.1
"
SLOT="5"
MYOPTIONS="
    designer [[ description = [ Install Qt designer plugins ] ]]
    ( mysql postgresql sqlite ) [[ number-selected = exactly-one ]]
"

KF5_MIN_VER=5.44.0
QT_MIN_VER=5.9.0

DEPENDENCIES+="
    build:
        dev-libs/libxslt [[ note = [ xlstproc ] ]]
    build+run:
        dev-libs/boost[>=1.34.0]
        dev-libs/libxml2:2.0
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-misc/shared-mime-info[>=1.3]
        designer? (
            kde-frameworks/kdesignerplugin:5[>=${KF5_MIN_VER}]
            x11-libs/qttools:5
        )
        sqlite? ( dev-db/sqlite[>=3.6.23] )
        !kde-frameworks/akonadi:5 [[
            description = [ Akonadi client libs have been merged back to the server ]
            resolution = uninstall-blocked-after
        ]]
        !server-pim/akonadi:0 [[
            description = [ Uninstall server-pim/akonadi:0 after switching to the slotted version ]
            resolution = uninstall-blocked-after
        ]]
        !server-pim/akonadi:4[<1.13.0-r5] [[
            description = [ File collision with earlier versions in the :4 slot ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-libs/libxml2:2.0 [[ note = [ xmllint is needed to run two tests ] ]]
    run:
        mysql? (
            virtual/mysql[>=5.1]
            x11-libs/qtbase:5[>=${QT_MIN_VER}][mysql]
        )
        postgresql? (
            dev-db/postgresql
            x11-libs/qtbase:5[>=${QT_MIN_VER}][postgresql]
        )
        sqlite? ( x11-libs/qtbase:5[>=${QT_MIN_VER}][sqlite] )
"

# Many tests need a running X server
RESTRICT="test"

akonadi_src_configure() {
    local cmakeparams=(
        -DCONFIG_INSTALL_DIR=/etc/xdg
        -DENABLE_ASAN:BOOL=FALSE
    )

    cmakeparams+=(
        $(cmake_option sqlite AKONADI_BUILD_QSQLITE)
        $(cmake_disable_find designer Qt5Designer)
        $(cmake_disable_find designer KF5DesignerPlugin)
    )

    if option mysql ; then
        cmakeparams+=( -DDATABASE_BACKEND:STRING=MYSQL )
    elif option postgresql ; then
        cmakeparams+=( -DDATABASE_BACKEND:STRING=POSTGRES )
    else
        cmakeparams+=( -DDATABASE_BACKEND:STRING=SQLITE )
    fi

    ecmake "${cmakeparams[@]}"
}

