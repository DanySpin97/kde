# Copyright 2012 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]

export_exlib_phases src_compile src_install

SUMMARY="Qt bindings for telepathy logger"
HOMEPAGE="https://commits.kde.org/${PN}"
DOWNLOADS=""

BUGS_TO="mixi@exherbo.org"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=2.5]
        kde-frameworks/extra-cmake-modules[>=1.6.0]
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5 [[ note = [ qhelpgenerator ] ]]
        )
    build+run:
        dev-libs/dbus-glib:1
        dev-libs/glib:2
        dev-libs/libxml2:2.0
        net-im/telepathy-glib[>=0.16.0]
        net-im/telepathy-logger:=[>=0.8.0]
        net-im/telepathy-qt[>=0.9.1][qt5(+)]
        sys-apps/dbus
        x11-libs/qtbase:5[>=5.2.0][glib]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
)

telepathy-logger-qt_src_compile() {
    default

    option doc && emake doxygen-doc
}

telepathy-logger-qt_src_install() {
    default

    option doc && dodoc -r doc/*
}

