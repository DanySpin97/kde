# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/src" ] kde [ translations=qt ]

SUMMARY="A property editing framework with editor widget similar to Qt Designer"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2"
SLOT="0"
MYOPTIONS="doc"

KF5_MIN_VER=5.16.0

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            dev-lang/perl:*
            x11-libs/qttools:5 [[ note = [ qhelpgenerator ] ]]
        )
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.4.0][gui]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Don't install anything so disable building them
    -DBUILD_EXAMPLES:BOOL=FALSE
    -DKPROPERTY_KF:BOOL=TRUE
    -DKPROPERTY_WIDGETS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

