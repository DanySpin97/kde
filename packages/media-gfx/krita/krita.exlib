# Copyright 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/$(ever range -3)" suffix="tar.gz" ]
require kde [ translations='ki18n' ]
require python [ blacklist='2' with_opt=true multibuild=false ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="A paint application for raster images"

HOMEPAGE="https://krita.org/"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2 LGPL-2"
SLOT="0"
MYOPTIONS="
    fftw     [[ description = [ Support for fast convolution operators ] ]]
    hdr      [[ description = [ Color management for HDR painting functionality ] ]]
    heif     [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    openexr
    pdf
    raw      [[ description = [ Enable reading RAW image files from digital photo cameras ] ]]
    sound    [[ description = [ Provides sound support for animations via QtMultimedia ] ]]
    tiff
    vectorization [[ description = [ Vectorize code to make use of parallel execution ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

KF5_MIN_VER="5.22.0"
QT_MIN_VER="5.6.0"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        virtual/pkg-config
    build+run:
        dev-libs/boost[>=1.55]
        graphics/exiv2[>=0.16]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]   [[ note = [ could be optional ] ]]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        media-libs/giflib
        media-libs/lcms2[>=2.4]
        media-libs/libpng:=
        sci-libs/eigen:3[>=3.0]
        sci-libs/gsl[>=1.7]
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXi
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui][-opengles] [[
            note = [ Fails to build with [opengles]: https://bugs.kde.org/show_bug.cgi?id=357708 ]
        ]]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        x11-utils/xcb-util   [[ note = [ XCB COMPONENTS ... ATOM ] ]]
        fftw? ( sci-libs/fftw[>=3.2] )
        hdr? ( media-libs/opencolorio )
        heif? ( media-libs/libheif[>=1.2.0] )
        openexr? (
            media-libs/ilmbase
            media-libs/openexr
        )
        pdf? ( app-text/poppler[qt5] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        python? (
            dev-python/sip[>=4.18.0][python_abis:*(-)?]
            dev-python/PyQt5[>=5.6.0][python_abis:*(-)?]
        )
        raw? ( media-libs/libraw[>=0.16] )
        sound? ( x11-libs/qtmultimedia:5[>=${QT_MIN_VER}] )
        tiff? ( media-libs/tiff )
        vectorization? ( dev-libs/vc )
        !app-office/calligra[<2.90][kde_parts:krita] [[
            description = [ media-gfx/krita was part of app-office/calligra in previous releases ]
            resolution = uninstall-blocked-after
        ]]
"

# gazillion of tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # "binary release build that can package some extra things like color themes"
    -DFOUNDATION_BUILD:BOOL=FALSE
    # "Some packaged scripts are not compatible with Python 2 and this should
    # only be used if you really have to use 2.7."
    -DENABLE_PYTHON_2:BOOL=FALSE
    -DKRITA_DEVS:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'fftw FFTW3'
        'hdr OCIO'
        HEIF
        OpenEXR
        'pdf Poppler'
        'python PythonLibrary'
        'python SIP'
        'python PyQt5'
        'raw LibRaw'
        'sound Qt5Multimedia'
        TIFF
        'vectorization Vc'
)

krita_src_prepare() {
    kde_src_prepare

    edo sed -e "/pyqt_sip_dir =/ s:sys.prefix:\"/usr\":" \
            -i cmake/modules/FindPyQt5.py
}

krita_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

krita_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

