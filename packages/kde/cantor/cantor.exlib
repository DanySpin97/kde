# Copyright 2011 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache
# Since 15.04.0 cantor gained a Python3 backend, but the FindPythonLibs3 cmake
# module isn't really useful when dealing with multiple Python versions.
require python [ blacklist='3' with_opt=true multibuild=false ]

export_exlib_phases src_configure pkg_postinst pkg_postrm

SUMMARY="KDE Frontend to Mathematical Software"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS="
    analitza  [[ description = [ Build the analitza (kalgebra) backend ] ]]
    julia     [[ description = [ Build Julia backend, a programming language for technical computing ] ]]
    lua       [[ description = [ Build the lua scripting backend ] ]]
    qalculate [[ description = [ Build the qalculate backend ] ]]
    R         [[ description = [ Build the R backend ] ]]
    ( analitza lua qalculate R ) [[ number-selected = at-least-one ]]
"

# FIXME: Find out what a module is used for and provide a usable description
# cantor needs one of these backends to be usable:
# Octave: http://www.octave.org/
# Sage: http://www.sagemath.org/
# Maxima: http://maxima.sourceforge.net/

KF5_MIN_VER="5.15.0"
QT_MIN_VER="5.6.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-text/libspectre
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpty:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:5[>=${QT_MIN_VER}]
        analitza? ( kde/analitza:5 )
        julia? ( sci-lang/julia )
        lua? ( dev-lang/LuaJIT )
        qalculate? ( sci-libs/libqalculate:= )
        R? (
            sci-lang/R[shared]
            sys-libs/libgfortran:=
        )
    suggestion:
        sci-apps/maxima
        sci-apps/octave
        sci-apps/sage
"

# 2 of 2 tests need a running X server
RESTRICT="test"

cantor_src_configure() {
    configure_one_multibuild
}

configure_one_multibuild() {
    # TODO: Fix FindPythonLibs3.cmake to allow configuring for the selected
    # PYTHON_ABI.
    local cmakeparams=(
        $(kf5_shared_cmake_params)
        -DPYTHON_EXECUTABLE=/usr/$(exhost --target)/bin/${PYTHON}
        -DWITH_libspectre:BOOL=TRUE
        $(cmake_disable_find analitza Analitza5)
        $(cmake_disable_find Julia)
        $(cmake_disable_find lua LuaJIT)
        $(cmake_disable_find Qalculate)
        $(cmake_disable_find R R)
    )

    if option python ; then
        if [[ $(python_get_abi) == 3.* ]] ; then
            cmakeparams+=(
                -DCMAKE_DISABLE_FIND_PACKAGE_PythonLibs:BOOL=TRUE
                -DCMAKE_DISABLE_FIND_PACKAGE_PythonLibs3:BOOL=FALSE
            )
        else
            cmakeparams+=(
                -DCMAKE_DISABLE_FIND_PACKAGE_PythonLibs:BOOL=FALSE
                -DCMAKE_DISABLE_FIND_PACKAGE_PythonLibs3:BOOL=TRUE
            )
        fi
    else
        cmakeparams+=(
            -DCMAKE_DISABLE_FIND_PACKAGE_PythonLibs:BOOL=TRUE
            -DCMAKE_DISABLE_FIND_PACKAGE_PythonLibs3:BOOL=TRUE
        )
    fi

    ecmake \
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        "${CMAKE_SRC_CONFIGURE_TESTS[@]}" \
        "${cmakeparams[@]}"
}

cantor_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

cantor_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

