# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A KDE GUI for alarm messages"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER=5.47.0
QT_MIN_VER=5.9.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
    build+run:
        kde/kdepim-apps-libs[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/mailcommon[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/kalarmcal:5[>=${PV}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcalcore:5[>=${PV}]
        kde-frameworks/kcalutils:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=${PV}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kholidays:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        media-libs/phonon[qt5]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        !kde/kdepim [[
            description = [ kalarm has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
"

kalarm_pkg_postinst() {
    gtk-icon-cache_pkg_postinst
}

kalarm_pkg_postrm() {
    gtk-icon-cache_pkg_postrm
}

