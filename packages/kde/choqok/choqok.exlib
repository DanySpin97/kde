# Copyright 2009, 2010, 2011 Ingmar Vanhassel
# Copyright 2015-2017 Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/$(ever range -2)/src" ] kde gtk-icon-cache

SUMMARY="A Free/Open Source micro-blogging client for K Desktop Environment"
DESCRIPTION="
choqoK is a quick and efficient micro-blogging client for the K Desktop
Environment. It currently supports the Twitter.com and Identi.ca services. The
name comes from an ancient Persian word that means sparrow.
"
HOMEPAGE="https://choqok.gnufolks.org/"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}2013/09/${PN}-${PV/./-}-released/"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    imstatus  [[ description = [ Sets status of your instant messenger as status in your micro-blog ] ]]
    konqueror [[ description = [ Plugin to easily share stuff when using Konqueror ] ]]
"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
        virtual/pkg-config
    build+run:
        app-crypt/qca[qt5]
        kde-frameworks/attica:5
        kde-frameworks/kcmutils:5
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kemoticons:5
        kde-frameworks/kglobalaccel:5
        kde-frameworks/kguiaddons:5
        kde-frameworks/khtml:5
        kde-frameworks/ki18n:5
        kde-frameworks/kio:5
        kde-frameworks/kjobwidgets:5
        kde-frameworks/knotifications:5
        kde-frameworks/knotifyconfig:5
        kde-frameworks/kservice:5
        kde-frameworks/ktextwidgets:5
        kde-frameworks/kwallet:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kxmlgui:5
        kde-frameworks/sonnet:5
        imstatus? ( net-im/telepathy-qt[qt5(+)] )
        konqueror? (
            kde-frameworks/kdewebkit:5
            kde-frameworks/kparts:5
        )
"

if ever at_least scm ; then
    QT_MIN_VER=5.8.0

    DEPENDENCIES+="
        build+run:
            x11-libs/qtbase:5[>=${QT_MIN_VER}]
            x11-libs/qtnetworkauth:5[>=${QT_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            sys-auth/qoauth[>=2.0.0][qt5(+)]
            x11-libs/qtbase:5
    "
fi
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'imstatus TelepathyQt5'
    'konqueror KF5Parts'
    'konqueror KF5WebKit'
)

