# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="K control module to regulate the screen's gamma correction"

LICENCES="GPL-2"
SLOT="4"
MYOPTIONS=""

if ever at_least 5.13.90 ; then
    KF5_MIN_VER="5.50.0"
    QT_MIN_VER="5.11.0"
else
    KF5_MIN_VER="5.42.0"
    QT_MIN_VER="5.9.0"
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        x11-libs/libX11
        x11-libs/libXxf86vm
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        !kde/kgamma [[
            description = [ kgamma has been renamed to kgamma5 to avoid a decreasing version number ]
            resolution = uninstall-blocked-after
        ]]
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
"

