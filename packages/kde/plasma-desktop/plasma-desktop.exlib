# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Plasma desktop"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2"
SLOT="4"
MYOPTIONS="
    file-search [[ description = [ Build the file search K Control module ] ]]
    ibus        [[ description = [ Build kimpanel, an input widget, and its IBus backend ] ]]
    input-kcm   [[ description = [ Configure input devices with the input KC(ontrol)M(odule) ] ]]
    synaptics   [[ description = [ Build KCM to configure Synaptics touchpads ] ]]
    ( providers: eudev systemd ) [[
        *description = [ udev provider ]
        number-selected = exactly-one
    ]]
"

if ever at_least 5.13.90 ; then
    KF5_MIN_VER=5.50.0
    QT_MIN_VER=5.11.0
else
    MYOPTIONS+=" pulseaudio"

    KF5_MIN_VER=5.45.0
    QT_MIN_VER=5.10.0
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
        input-kcm? (
            x11-drivers/xf86-input-evdev[>=2.8.99.1]
            x11-drivers/xf86-input-libinput
        )
    build+run:
        dev-libs/boost
        kde/breeze:4[>=$(ever range -3)]
        kde/kwin:${SLOT}
        kde/plasma-workspace:${SLOT}
        kde-frameworks/attica:5[>=${KF5_MIN_VER}]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kactivities-stats[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kemoticons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpeople:5[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kunitconversion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/phonon[>=4.6.60][qt5]
        x11-apps/xkeyboard-config
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXfixes
        x11-libs/libXft
        x11-libs/libXi[>=1.2.0]
        x11-libs/libxkbfile
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql] [[ note = [ sql for KActivitiesStats, will move to kactivities if ready ] ]]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        x11-utils/xcb-util-image
        x11-utils/xcb-util-keysyms
        x11-utils/xcb-util-wm
        file-search? ( kde-frameworks/baloo:5[>=5.15] )
        ibus? (
            dev-libs/glib:2
            inputmethods/ibus[>=1.5.0]
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        synaptics? (
            kde-frameworks/kded:5[>=${KF5_MIN_VER}]
            x11-drivers/xf86-input-synaptics
        )
        !kde/kde-runtime[<15.12.1-r1] [[
            description = [ File collisions between ported and unported apps ]
            resolution = uninstall-blocked-after
        ]]
        !kde/kde-workspace[<4.11.11-r2][kf5-coinstall] [[
            description = [ solid-action-desktop-gen is installed by < kde-workspace-4.11.11-r2 ]
            resolution = upgrade-blocked-before
        ]]
        !kde-frameworks/kactivities:5[<5.20] [[
            description = [ The Activities KCM has been moved to plasma-desktop ]
            resolution = upgrade-blocked-before
        ]]
    run:
        kde/kde-cli-tools:${SLOT} [[ note = [ keditfiletype, kcmshell5 ] ]]
        sys-apps/accountsservice  [[ note = [ called via dbus in kcm_useraccount ] ]]
        x11-apps/mkfontdir
        x11-apps/setxkbmap
        x11-apps/xmodmap
        x11-apps/xrdb
        x11-libs/qtgraphicaleffects:5
        x11-libs/qtquickcontrols:5
    suggestion:
        dev-libs/libunity [[
            description = [ Show application progress indicators for apps
                            supporting the Unity Launcher API ]
        ]]
        kde/oxygen [[
            description = [ Notifications sounds and Oxygen theme for KDE/Qt5 applications ]
        ]]
"

if ever at_least 5.13.90 ; then
    :
else
    DEPENDENCIES+="
        build+run:
            pulseaudio? (
                dev-libs/glib:2
                media-libs/libcanberra[pulseaudio] [[ description = [ needed for speaker setup GUI ] ]]
                media-sound/pulseaudio[>=0.9.16]
            )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        PulseAudio
        'pulseaudio Canberra'
    )
fi

# 4 of 4 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_AppStreamQt:BOOL=TRUE
    # Hard disable the scim backend for kimpanel, since scim upstream seems
    # dead, prefer the newer ibus backend
    -DCMAKE_DISABLE_FIND_PACKAGE_SCIM:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'file-search KF5Baloo'
    IBus
    'ibus GLIB2'
    'ibus GIO'
    'ibus GObject'
    'input-kcm Evdev'
    'input-kcm XorgLibinput'
    Synaptics
)

plasma-desktop_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

plasma-desktop_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

