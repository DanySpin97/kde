# Copyright 2012-2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_USER=QupZilla
MY_PNV=${MY_USER}-${PV}

require github [ user=${MY_USER} release=v${PV} suffix=tar.xz ] qmake
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_configure src_install pkg_postinst pkg_postrm

SUMMARY="QupZilla is modern web browser based on WebKit core and Qt Framework"
HOMEPAGE="https://www.qupzilla.com"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    bash-completion
    gnome [[ description = [ Support for storing passwords in Gnome keyring ] ]]
    kde [[ description = [ Support for storing passwords in kwallet ] ]]
    zsh-completion
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

QT5_MIN_VER="5.9.0"

# There is NO_X11, so in principle, it would be possible to build qupzilla without support for
# X11, but as qt5 links to libX11 currently, there is no point in doing so.
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-spell/hunspell:=
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT5_MIN_VER}][gui][sql][sqlite] [[ note = [ gui-private, network, printsupport, sql, widgets ] ]]
        x11-libs/qtdeclarative:5[>=${QT5_MIN_VER}] [[ note = [ quickwidgets ] ]]
        x11-libs/qtscript:5[>=${QT5_MIN_VER}] [[ note = [ script ] ]]
        x11-libs/qtwebchannel:5[>=${QT5_MIN_VER}]
        x11-libs/qtwebengine:5[>=${QT5_MIN_VER}] [[ note = [ webenginewidgets ] ]]
        x11-libs/qtx11extras:5[>=${QT5_MIN_VER}]
        gnome? ( gnome-desktop/gnome-keyring )
        kde? ( kde-frameworks/kwallet:5 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    run:
        kde-frameworks/breeze-icons:5 [[ note = [ Used in preferences ] ]]
"

EQMAKE_SOURCES=( ${MY_USER}.pro )

qupzilla_src_configure() {
    option kde && edo export KDE_INTEGRATION="true" || edo export KDE_INTEGRATION="false"
    option gnome && edo export GNOME_INTEGRATION="true" || edo export GNOME_INTEGRATION="false"

    edo export QUPZILLA_PREFIX=/usr/$(exhost --target)
    edo export SHARE_FOLDER=/usr/share

    eqmake 5
}

qupzilla_src_install() {
    default

    if ! option bash-completion; then
        edo rm "${IMAGE}"/usr/share/bash-completion/completions/qupzilla
        edo rmdir "${IMAGE}"/usr/share/bash-completion/{completions,}
    fi

    if option zsh-completion; then
        dodir /usr/share/zsh/site-functions/
        insinto /usr/share/zsh/site-functions/
        doins linux/completion/_qupzilla
    fi
}

qupzilla_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

qupzilla_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

