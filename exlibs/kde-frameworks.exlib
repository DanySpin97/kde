# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the GNU General Public License

# Exlib for all packages released together as KDE Frameworks. See
# sets/kde-frameworks.conf for all packages which should use this exlib.
#
# Exparams:
#   docs ( format: true or false ) ( defaults to true )
#     Whether the package can build API docs in the QCH format. Adds an
#     option and optional dependencies on doxygen and qttools.

# Name of the package
myexparam pn=${MY_PN:-${PN}}

require kde.org [ pn=$(exparam pn) ]

myexparam -b docs=true

if ! ever is_scm ; then
    case "$(exparam pn)" in
        # Deprecated frameworks considered as porting aid only
        kdelibs4support|khtml|kjs|kjsembed|kmediaplayer| kross)
            DOWNLOADS="mirror://kde/stable/frameworks/$(ever range -2)/portingAids/$(exparam pn)-${PV}.tar.xz"
            ;;
        *)
            DOWNLOADS="mirror://kde/stable/frameworks/$(ever range -2)/$(exparam pn)-${PV}.tar.xz"
            ;;
    esac
fi

UPSTREAM_RELEASE_NOTES="https://kde.org/announcements/kde-frameworks-${PV}.php"

SLOT=5
# Upstream's rule: 5.(n-2).0 with n="latest major version" is the minimal Qt5
# dependency, or just grep for REQUIRED_QT_VERSION.
QT_MIN_VER="5.8.0"

if ever at_least scm ; then
    KF5_MIN_VER="5.51.0"
else
    KF5_MIN_VER="${PV}"
fi

DEPENDENCIES+="
    build+run:
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

if exparam -b docs ; then
    MYOPTIONS+="
        doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
    "

    DEPENDENCIES+="
        build:
            doc? (
                app-doc/doxygen
                x11-libs/qttools:5   [[ note = qhelpgenerator ]]
            )
    "

    CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )
fi

